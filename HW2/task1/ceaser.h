#ifndef SPL_CEASER_H
#define SPL_CEASER_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char shift_letter(char,int);
char * shift_string(char[],int);

#endif 
