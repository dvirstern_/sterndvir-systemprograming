#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


int main(int argc, char ** argv)
{
	FILE *fp = fopen ( *argv, "r");
	int count = 0;
	srand (time(NULL));
	int randomLine = rand (10) + 1;
	printf ("line is : %d\n",randomLine);
	if (fp != NULL)
	{
		char line [256]; //max size of a line
		while (fgets(line , 256, fp) != NULL)
		{
			if (count == randomLine)
			{
				fclose (fp);
				printf ("%s",line);
			}
			else{
				count++;}
		}
	}
	else
	{
		printf("NAME\n   rancat - print random line\n\nSYNOPSIS\n    rancat FILE\n\nDESCRIPTION\n    print a random line from the FILE on the standard output.");
	}
	
	fclose (fp);
	return 0;
}
